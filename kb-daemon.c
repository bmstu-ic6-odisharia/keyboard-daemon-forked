#include <sys/types.h>
#include <sys/stat.h>
#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <errno.h>
#include <unistd.h>
#include <syslog.h>
#include <string.h>
#include <linux/input.h> //for input struct, event codes
#include <signal.h>
#include <syslog.h>

/* for timer */
#include <signal.h>
#include <time.h>
#include <sys/types.h>
#include <sys/stat.h>

#define SIZE_OF_ARRAY(_array) (sizeof(_array) / sizeof(_array[0]))

typedef struct key_struct {
    unsigned int        keycode;
    long                count;
    struct key_struct*  next;
} key_node;
key_node *first = NULL, *current = NULL;

timer_t timerid;
struct itimerspec its;

static void terminate_process() {
    syslog(LOG_INFO, "kb-daemon terminated");
    closelog();
    exit(EXIT_SUCCESS);
}

static void beforeexit(int sig){
    its.it_value.tv_sec = 0;
    its.it_value.tv_nsec = 0;
    its.it_interval.tv_sec = its.it_value.tv_sec;
    its.it_interval.tv_nsec = its.it_value.tv_nsec;

    timer_settime(timerid, 0, &its, NULL);

    current = first;
    while(current != NULL) {
        key_node* freemem = current;
        current = current -> next;
        free(freemem);
    }
    exit(EXIT_SUCCESS);
}

void add_symbol(unsigned short event_keycode) {
    if (first == NULL){
        current = malloc(sizeof(key_node));
        current->keycode    = event_keycode;
        current->count      = 0;
        current->next       = NULL;
        first = current;
    }

    current = first;
    while (current->keycode != event_keycode && current->next != NULL) {
        current = current->next;
    }

    if (current->keycode != event_keycode) {
        key_node* next = malloc(sizeof(key_node));

        current->next = next;
        current = next;

        current->keycode = event_keycode;
        current->count = 1;
        current->next = NULL;

        syslog(LOG_INFO, "New keycode: %d\n", current->keycode);
    }
    else {
        current->count++;
    }
}

static void writetofile(int sig, siginfo_t *si, void *uc){
    syslog(LOG_INFO, "I am writing file...\n");
    int filedesc = open("newgrabber.txt", O_CREAT | O_WRONLY | O_APPEND);

    if (filedesc < 0) {
        exit(EXIT_FAILURE);
    }
    char buf[40];
    memset(buf, 0, sizeof(buf));

    time_t current_time = time(NULL);
    struct tm tm = *localtime(&current_time);
    sprintf(buf, "At: %d-%d-%d %d:%d:%d\n", tm.tm_year + 1900, tm.tm_mon + 1, tm.tm_mday, tm.tm_hour, tm.tm_min, tm.tm_sec);

    write(filedesc, buf, strlen(buf));

    current = first;
    /* Not optimal, but ... */
    while (current != NULL) {
        memset(buf, 0, sizeof(buf));

        snprintf(buf, sizeof(current->keycode), "%d", current->keycode);

        write(filedesc, buf, strlen(buf));
        write(filedesc, " - ", strlen(" - "));

        snprintf(buf, sizeof(current->count), "%ld", current->count);

        write(filedesc, buf, strlen(buf));
        write(filedesc, "\n", strlen("\n"));

        key_node* freemem = current;
        current = current->next;

        free(freemem);
        first = NULL;
    }
    write(filedesc, "--------------------\n", 21);
}


void newtimer(unsigned long secs){
    struct sigevent sev;
    sigset_t mask;
    struct sigaction sa;

    /* make handler */
    sa.sa_flags = SA_SIGINFO; //signal
    sa.sa_sigaction = writetofile; //handler
    sigemptyset(&sa.sa_mask);
    if(sigaction(SIGRTMIN, &sa, NULL) == -1) {
        exit(EXIT_FAILURE);
    }

    /*block timer signals */
    sigemptyset(&mask);
    sigaddset(&mask, SIGRTMIN);
    if(sigprocmask(SIG_SETMASK, &mask, NULL) == -1) {
        exit(EXIT_FAILURE);
    }

    /* create timer */
    sev.sigev_notify = SIGEV_SIGNAL;
    sev.sigev_signo = SIGRTMIN;
    sev.sigev_value.sival_ptr = &timerid;
    if (timer_create(CLOCK_REALTIME, &sev, &timerid) == -1) {
        syslog(LOG_INFO, "ERROR :( exiting...\n)");
        exit(EXIT_FAILURE);
    }

    /*start timer */
    its.it_value.tv_sec = secs;
    its.it_value.tv_nsec = 0;
    its.it_interval.tv_sec = its.it_value.tv_sec;
    its.it_interval.tv_nsec = its.it_value.tv_nsec;
    if (timer_settime(timerid, 0, &its, NULL) == -1) {
        syslog(LOG_INFO, "ERROR :( exiting...\n)");
        exit(EXIT_FAILURE);
    }

    /* Unblock */
    if(sigprocmask(SIG_UNBLOCK, &mask, NULL) == -1) {
        exit(EXIT_FAILURE);
    }
}

int main(void) {

    /* Our process ID and Session ID */
    pid_t pid, sid;

    /* Fork off the parent process */
    pid = fork();
    if (pid < 0) {
            exit(EXIT_FAILURE);
    }
    /* If we got a good PID, then
       we can exit the parent process. */
    if (pid > 0) {
            exit(EXIT_SUCCESS);
    }

    /* Open syslog */
    openlog("kb-daemon-log", LOG_PID|LOG_CONS, LOG_USER);
    syslog(LOG_INFO, "kb-daemon started with PID %d", (int) getpid());

    /* Add nadler for SIGTERM signal */
    signal(SIGTERM, terminate_process);

    /* Change the file mode mask */
    umask(0);

    /* Open any logs here */

    /* Create a new SID for the child process */
    sid = setsid();
    if (sid < 0) {
            /* Log the failure */
            exit(EXIT_FAILURE);
    }

    /* Close out the standard file descriptors */
    close(STDIN_FILENO);
    close(STDOUT_FILENO);
    close(STDERR_FILENO);

    /* Daemon-specific initialization goes here */

    const char kbd_event [] = "/dev/input/by-path/platform-i8042-serio-0-event-kbd";
    struct input_event event;
    int file_desc = open(kbd_event, O_RDONLY);
    int fread;

    /* free mem before exit */
    signal(SIGINT, beforeexit);
    newtimer(5);

    /* The Big Loop */
    while (1) {
        /* Do some task here ... */
        fread = read(file_desc, &event, sizeof(event));
        if((fread == sizeof(event)) && (event.type == EV_KEY) && (event.value == 0)) { // checke read && keyboard's key && key released
            add_symbol(event.code);
        }
    }
    closelog();
    exit(EXIT_SUCCESS);
}
